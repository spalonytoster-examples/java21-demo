package com.example.java21demo.switches;

public class EventListenerV2 {

    private final CustomerAddToCartEventHandler customerAddToCartEventHandler = new CustomerAddToCartEventHandler();
    private final CustomerCheckoutEventHandler customerCheckoutEventHandler = new CustomerCheckoutEventHandler();

    String receive(IncomingEvent event) {
        // Usage of capture groups
        if (event instanceof CustomerAddToCartEvent addToCartEvent) {
            return customerAddToCartEventHandler.handle(addToCartEvent);
        } else if (event instanceof CustomerCheckoutEvent checkoutEvent) {
            return customerCheckoutEventHandler.handle(checkoutEvent);
        } else {
            return "Unknown event";
        }
    }
}
