package com.example.java21demo.switches;



public class CustomerAddToCartEventHandler implements EventHandler<CustomerAddToCartEvent> {
    @Override
    public String handle(CustomerAddToCartEvent event) {
        return "CustomerAddToCartEvent";
    }
}
