package com.example.java21demo.switches;

public interface EventHandler<T> {

    String handle(T event);
}
