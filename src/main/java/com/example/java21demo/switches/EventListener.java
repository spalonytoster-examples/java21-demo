package com.example.java21demo.switches;

public class EventListener {

    private final CustomerAddToCartEventHandler customerAddToCartEventHandler = new CustomerAddToCartEventHandler();
    private final CustomerCheckoutEventHandler customerCheckoutEventHandler = new CustomerCheckoutEventHandler();

    String receive(IncomingEvent event) {
        return switch (event) {
            case CustomerAddToCartEvent addToCartEvent -> customerAddToCartEventHandler.handle(addToCartEvent);
            case CustomerCheckoutEvent checkoutEvent -> customerCheckoutEventHandler.handle(checkoutEvent);
            default -> "Unknown event";
        };
    }
}
