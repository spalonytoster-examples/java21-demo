package com.example.java21demo.switches;

public class CustomerCheckoutEventHandler implements EventHandler<CustomerCheckoutEvent> {

    @Override
    public String handle(CustomerCheckoutEvent event) {
        return "CustomerCheckoutEvent";
    }
}
