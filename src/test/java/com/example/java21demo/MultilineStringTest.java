package com.example.java21demo;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class MultilineStringTest {

    @Test
    void multiline1() {
        var shakespeare = """
                To be, or not to be: that is the question:
                Whether 'tis nobler in the mind to suffer
                The slings and arrows of outrageous fortune,
                Or to take arms against a sea of troubles,
                And by opposing end them? To die: to sleep
        """;
        assertThat(shakespeare).doesNotStartWith("T");

        shakespeare = shakespeare.stripLeading();
        assertThat(shakespeare).startsWith("T");
    }

    @Test
    void multiline2() {
        var shakespeare = """
        To be, or not to be: that is the question:
        Whether 'tis nobler in the mind to suffer
        The slings and arrows of outrageous fortune,
        Or to take arms against a sea of troubles,
        And by opposing end them? To die: to sleep
        """;
        assertThat(shakespeare).startsWith("T");
    }
    @Test
    void multiline3() {
        var shakespeare = """
                        To be, or not to be: that is the question:
                        Whether 'tis nobler in the mind to suffer
                        The slings and arrows of outrageous fortune,
                        Or to take arms against a sea of troubles,
                        And by opposing end them? To die: to sleep
                        """;
        assertThat(shakespeare).startsWith("T");
    }
}