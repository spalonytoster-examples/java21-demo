package com.example.java21demo.switches;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class EventListenerV2Test {

    private EventListenerV2 eventListener;

    @Test
    void testSwitch() {
        eventListener = new EventListenerV2();

        assertThat(eventListener.receive(new CustomerAddToCartEvent()))
                .isEqualTo("CustomerAddToCartEvent");

        assertThat(eventListener.receive(new CustomerCheckoutEvent()))
                .isEqualTo("CustomerCheckoutEvent");

        assertThat(eventListener.receive(new IncomingEvent() {}))
                .isEqualTo("Unknown event");
    }
}