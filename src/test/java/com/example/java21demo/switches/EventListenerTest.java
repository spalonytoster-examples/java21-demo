package com.example.java21demo.switches;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class EventListenerTest {

    private EventListener eventListener;

    @Test
    void testSwitch() {
        eventListener = new EventListener();

        assertThat(eventListener.receive(new CustomerAddToCartEvent()))
                .isEqualTo("CustomerAddToCartEvent");

        assertThat(eventListener.receive(new CustomerCheckoutEvent()))
                .isEqualTo("CustomerCheckoutEvent");

        assertThat(eventListener.receive(new IncomingEvent() {}))
                .isEqualTo("Unknown event");
    }

}